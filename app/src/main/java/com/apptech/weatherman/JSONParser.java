package com.apptech.weatherman;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class JSONParser {

    String charset = "UTF-8";
    HttpURLConnection conn;
    DataOutputStream wr;
    StringBuilder result;
    URL urlObj;
    JSONObject jObj = null;
    JSONArray jArray = null;
    StringBuilder sbParams;

    public JSONObject   makeHttpRequest(String url, String method,
                                      HashMap<String, String> params) {
        String token="";
        sbParams = new StringBuilder();
        int i = 0;
        for (String key : params.keySet()) {
            try {
                if (i != 0) {
                    sbParams.append("&");
                }
                Log.d("TAGG",key);
                if(!key.equals("token"))// Ensure the token is not shown or added in the URL's GET request
                sbParams.append(key).append("=")
                        .append(URLEncoder.encode(params.get(key), charset));
                else
                    token = params.get(key);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;
        }

        if (method.equals("POST")) {
            // request method is POST
//            return sendJSONPOSTdata(url);
            try {

                urlObj = new URL(url);

//            HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                /**
                 * Authentication code start
                 * */
//                final String basicAuth = "Basic " + Base64.encodeToString((API_USERNAME+":"+API_PASSWORD).getBytes(), Base64.NO_WRAP);
                if(!token.equals("")){
                    final String basicAuth = "Token "+token;
                    conn.setRequestProperty("Authorization", basicAuth);
                }

                /**
                 * Authentication code ends
                 * */

                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("POST");

                JSONObject cred = new JSONObject();
                try {

                    for (String key : params.keySet()) {
                        cred.put(key, params.get(key));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            /*auth.put("tenantName", "adm");
            auth.put("passwordCredentials", cred.toString());

            parent.put("auth", auth.toString());*/
                Log.d("POST!!!!!!!!!!",cred.toString());
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(cred.toString());
                wr.flush();
                wr.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (method.equals("GET")) {
            // request method is GET

            if (sbParams.length() > 0) {
                url += "?" + sbParams.toString();
            }

            try {
                urlObj = new URL(url);

                conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);

                conn.setDoOutput(false);

                conn.setRequestMethod("GET");
                Log.d("request", "starting at url++++++" + url);
                conn.setRequestProperty("Accept-Charset", charset);

//                conn.setConnectTimeout(15000);

                conn.connect();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            //Receive the response from the server
            int status = conn.getResponseCode();
            BufferedInputStream in;
            if (status >= 400 ) {
                in = new BufferedInputStream( conn.getErrorStream() );
            } else {
                in = new BufferedInputStream( conn.getInputStream() );
            }
//            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            Log.d("JSON Parser", "result: " + result.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

        conn.disconnect();

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(result.toString());
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON Object
        return jObj;
    }


}